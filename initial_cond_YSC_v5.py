# -*- coding: utf-8 -*-
########################################################################################
# mtcarlo_3body: script to generate the initial conditions for binary-single scattering
# following Hut & Bahcall 1983
# co-authored by Marco Dall'Amico and Michela Mapelli in 2020 -> New version June 2021
#################################### LIBRARIES ########################################

from scipy.stats import norm
from matplotlib import gridspec
import numpy as np
import random as rnd
import math
import scipy.stats
import matplotlib.pyplot as plt
import os



###################################### PARAMETERS ######################################

check1  = True                                          #   create orbital parameter file
check2  = False                                         #   create input files
infoplot= False                                         #   show some informative plots

#   Conversion Constants
yr      =   3.1557600e7                                 #   seconds/yr
msun    =   1.98892e33                                  #   g/msun
AU      =   1.49598073e+13                              #   cm/AU
PC      =   3.08567758e+18                              #   cm/pc

#   Simulation Parameters
Ntot = 100000                                            #   number of systems to generate IC
Ngen = 0                                                #   add 10k for each set
Z    = 0.002                                            #   metallicity (0.1 solar)
SN_presc = 'Rapid'                                    #   change the initial mass distribution

Nseed = 1000                                               #   NB must be chaged at each new set
np.random.seed(Nseed)
rnd.seed(Nseed)                                          


#   Physics Parameters
G       = 6.6743015e-8 * (msun)*(yr)**2/(PC)**3         #   pc^3 M_sol^-1 yr^-2
Gnbody  = 1.0
L_scale = 1./1.                                         #   pc^-1
M_scale = 1./1.                                         #   M_sun^-1
T_scale = 1./((1./L_scale)**3/(G/M_scale))**(0.5)       #   fixed days^-1
V_scale = (L_scale/T_scale)                             #   days pc^-1
c       = 2.99792458e10*(yr/PC)                         #   cm/s * pc/yr conversion
c5      = c**5.



#integration time and step time fo ARWV
int_t  = 150000                                         #   years 
step_t = 100                                            #   years

Int_time  = (int_t)*T_scale                             #   (years)*days conversion*N-unit conversion
Step_time = (step_t)*T_scale                            #   days*N-unit conversion



print('ARWV_integrating_time (TMAX)     = ',Int_time,'ARWV_step_time  (DeltaT) = ',Step_time)
print('speed of light in N-units    = '+str(c*V_scale))


######################### EXTREMES of the DISTRIBUTIONS ################################

#----- intruder mass -----
#Mmin        =   60.                                     #   min mass in msun of the distribution


#---- semimajor axis-----
min_cluster =   8000                                    #   min mass in msun from the cluster sample

xi          =   3.                                      #   Quinlan 1996, Mapelli 2021, Dall'Amico 2021

m_mean      =   1.                                      #   average star mass in Msun
rho_ysc     =   10**(3.3)                               #   typical half-mass density Msol/pc^3 of a ysc from Mapelli at al. 2021
rho_core    =   20. * rho_ysc                           #   from Mapelli et al. 2021


#---- orbital phase------
fmin        =   0.0
fmax        =   2.*math.pi


#--- relative velocity ---
vmean       =   0.0
vstd        =   5.0                                     #   km/s

v_disp      =   vstd * 1e5 * (yr/PC)                    #   km/s --> cm/s --> pc/yr used in a_hard, a_gw
v_esc       =   2.   * v_disp                           #   escape velocity used in e_ej


#--- impact parameters ---
bmin2       =   0.0                                     #   pc^2
#bmax is adaptive


#---- psi angle------
psimin      =   0.0
psimax      =   2.*math.pi


#--- phi angle------
phimin      =   0.0
phimax      =   2.*math.pi


#---- cosinus of theta angle ----
costmin     =   -1.0
costmax     =   1.0


#----- spin -----
Svmean       =   0.0
Svstd        =   0.1


###################################### FUNCTIONS ######################################## 



def sign(x):        
    if x>=0.0:
        return int(1)
    else:
        return int(0)



#   https://docs.scipy.org/doc/numpy-1.15.0/reference/generated/numpy.random.random_sample.html#numpy.random.random_sample
def uniformedev(x,minimo,maximo):                       
    x*=(maximo-minimo)
    x+=minimo
    return x




def gauss(x1,x2,med,sigma):     
    p=sigma*np.sqrt(-2.*np.log(1.-x1))*np.cos(2.*math.pi*x2)    #   Box-Muller method
    p=p+med
    return p



    
def anomaly(x,ecc,E):
    g = E - ecc * np.sin(E) - x                         #   x is random 0,2P; g is function I must find the zeroes
    return g





def bisec(x,ecc,tol):
    E1=1.0
    E2=100.
    g2=100.
    while(abs(g2)>tol):
        delta=0.5*(E2+E1)
        g1 = anomaly(x,ecc,E1)
        g2 = anomaly(x,ecc,E2)
        if(sign(g1)== sign(g2)):                        #   if g1 and g2 have same sign search other points
            if(sign(g1)==1):
                if((g1)>(g2)):
                    E1 = E2
                    E2 = E2 + delta
                else:
                    E2 = E1
                    E1 = E1 - delta	
            else:
                if((g1)<(g2)):
                    E1 = E2
                    E2 = E2 + delta
                    
                else:
                    E2 = E1
                    E1 = E1 - delta         
        else:
            E3 = (E2 + E1)*0.5
            g3 = anomaly(x,ecc,E3)
            if(sign(g3)==sign(g1)):
                E1=E3
            else:
                E2=E3               
    return ((E1+E2)*0.5)





def rejection_sampling(M,N):
    samples = []
    Mmin    = np.amin(M)    
    Mmax    = np.amax(M)
    x       = np.linspace(Mmin,Mmax,N*100)                # <--- THIS PARAMETER REALLY SLOWS DOWN THE SCRIPT, REDUCE N*100
    kde     = scipy.stats.gaussian_kde(M,bw_method=0.075) # extract the KDE from Di Carlo Masses
    maximum = np.amax(kde(x))*1.1                         # find the max value of the KDE with 10% tolerance, THIS IS THE BOTTLENECK
    j       = 0
    while j<N:
        x_trial = uniformedev(rnd.random(),Mmin,Mmax)            # random M value in the domain of KDE
        y       = uniformedev(rnd.random(),0,maximum)            # random value in uniform dist at M
        if y<=kde(x_trial):                               # if y<KDE keeps the value
            samples.append(x_trial)     
            j+=1
        else:                                             # otherwise it rejects it                                               
            None
    samples = np.array(samples)
    return (samples)





#########################################################################################
                                     # MAIN #
#########################################################################################

#------------------------------------  BH MASSES ------------------------------------#

# first generation signle BHs from MOBSE

# first generation single BHs from MOBSE
if SN_presc == 'Delayed':                                                                                              # include BH in the low mass gap (3<m<5)
    Path2MOBSE = '/data3/mapelli/mobse_roche/vcm_mcartale_ngaspari/A1/single_BH_'+str(Z)+'.txt'                       
elif SN_presc == 'Rapid':                                                                                              # minimum mass is 5 Msol
    Path2MOBSE = '/data3/mapelli/mobse_roche/vcm_mcartale_ngaspari_rapid/A1/single_BH_'+str(Z)+'.txt'
else:
    print('Choose between Rapid or Delayed')
MBH  = np.genfromtxt(Path2MOBSE,skip_header=3,usecols=(2))

m1 = np.array([])
m2 = np.array([])
m3 = np.array([])

i=0
while(i<Ntot):
    binary = rnd.choices(MBH,k=2)
    m1 = np.append(m1,max(binary))
    m2 = np.append(m2,min(binary))
    
    single = rnd.choice(MBH)
    m3 = np.append(m3,single)
    
    i+=1

m1 *= M_scale
m2 *= M_scale
m3 *= M_scale

# find the peak value in the mass distribution from mobse of m3, will be used later in a_ej
H , edges = np.histogram(m3,bins=50)
mid_bins = np.array([])

for i in range(edges.size-1):
    mid = ( edges[i] + edges[i+1] )/ 2.
    mid_bins = np.append(mid_bins,mid)
    
indx = np.where(H==H.max())
Max_hist = mid_bins[indx]
peak_m3  = Max_hist[0]
print('max peak M  = '+str(peak_m3))

#-------------------------------------  ECCENTRICITY -------------------------------------- #

#generate binary orbital eccentricity based on thermal distribution P(e)\propto{}e^2

ecc=[]
i=0
while(i<Ntot):
    p=rnd.random()
    p=uniformedev(p,0.,1.)
    p=p**0.5
    ecc.append(p)
    i+=1
    
#------------------------------------  SEMI-MAJOR AXIS ------------------------------------ #

# NORMAL SAMPLE FROM GAUSSIAN FITTED ON Di Carlo et al. 2019 CLUSTERS LOG DISTR >8000 M_sol 20Myr in AU

dat     = np.genfromtxt('/home/dallamico/Lab_Computational_Physics/semi20_mass.txt')

A_hard = []
A_ej   = []
A_gw   = []
resampled = 0


# extract only data for massive clusters
i = 0
ugo20M = []
for i in range(dat[:,0].size):
    if dat[i,1]>=min_cluster:                                                               #   threshold: 8000 M_sol
        ugo20M.append(dat[i,0])


# go in log space and extract gaussian parameters
ugo20M = np.log10(ugo20M)
(mu, sigma) = norm.fit(ugo20M)
print(mu,sigma,np.power(10,mu),np.power(10,sigma))


# normal sample
samples = []
i=0
while i<Ntot:
    
    M1     =   max(m1[i],m2[i])
    M2     =   min(m1[i],m2[i])
    Mtot   =   M1 + M2
    u      =   M1*M2 / Mtot
    
    #   COMPUTE LIMITS (out in pc)
    
    #   max a to be considered an hard binary (derived from Gm1m2/2a = 0.5 <m> sigma^2 )
    a_hard  = ( G * M1 * M2 ) / ( m_mean * v_disp**2. )

    #   min a below which the binary is ejected from the cluster as a consequence of an hard interaction ( Miller & Hamilton 2002 eq.1, Mapelli 2021 eq.17, Dall'Amico 2021, eq 9)
    a_ej    =   ( xi * G * u ) * (peak_m3 / (Mtot * v_esc) )**2.

    #   max a below which gw become the main reduction mechanism (derived equating the hardening timescale with the gw timescale of Peters 1964, see Mapelli et al 2021. eq.18)
    a_gw    =   ( ((   32. * G**2. * M1 * M2 * Mtot * v_disp ) / ( 5. * math.pi * xi * (1-ecc[i]**2.)**(7./2.) * c5 * rho_core))  * (1 + (73./24.)*ecc[i]**2. + (37./96.)*ecc[i]**4) )**(1./5.)

    A_hard.append(a_hard)
    A_ej.append(a_ej)
    A_gw.append(a_gw)    
    
    #   Pc to AU conversion (di carlo uses AU)
    
    a_hard *= (PC/AU)
    a_ej   *= (PC/AU)
    a_gw   *= (PC/AU)
    
    
    a_min = np.log10(max(a_gw, a_ej))
    a_max = np.log10(a_hard)
    
    if a_min>a_max:
        print(i,np.power(10,a_min),np.power(10,a_max),'ERROR - a_min>a_max')
        resampled+=1
    
    trial=np.random.normal(mu,sigma)
    
    if trial>=a_min and trial<=a_max:                                                      
        samples.append(trial)
        i+=1


samples = np.power(10,samples)                                                              #   log(AU)-->AU
a = np.array(samples)*(AU/PC)*L_scale                                                       #   AU-->pc

print('Need to be resamples (min>max) = ',resampled)
print('Smajor-Axis: DONE!')



    

#------------------------------------ ORBITAL PHASE ----------------------------------------#

#generate psi angle and binary orbital phase based on Hut & Bahcall 1983 http://articles.adsabs.harvard.edu/pdf/1983ApJ...268..319H



tol=1e-7
Efinale=[]
fase=[]
s=0
i=0

while(i<Ntot):
    p=rnd.random()
    p=uniformedev(p,fmin,fmax)
    Efinale.append(bisec(p, ecc[i],tol))                                                    #   call bisection and solves eccentric anomaly
    f=2.0 * np.arctan((((1.+ecc[i])/(1.-ecc[i])))**0.5 * np.tan(Efinale[i]/2.))             #   convert to phasis
    if(Efinale[i]!=Efinale[-1]):
        print(Efinale[i],Efinale[-1])
    #if(s%2!=0):
    #    f=f+math.pi
    #+=1
    fase.append(f)
    i+=1
  

print('Ecc & Obrital Phase: DONE!')



################################## ENCOUNTER PROPERTIES #####################################



#-------------------------------------  RELATIVE VELOCITY ----------------------------------#  

#generate relative velocity module, based on Maxwellian with vstd=5 km/s typical of a YMSC


vel   = []
i=0

while(i<Ntot):
    p=[]
    j=0
    while(j<3):
        p1=rnd.random()
        p2=rnd.random()
        p.append(gauss(p1,p2,vmean,vstd))
        j+=1
    vel.append((p[0]*p[0]+p[1]*p[1]+p[2]*p[2])**0.5)
    i+=1

v = np.array(vel)                                                                           #   km/s


vel =(v*1e5)*(yr/PC)*V_scale                                                                #   km/s -> cm/s -> pc/yr -> N-units

print('Velocities: DONE!')

#------------------------------------- IMPACT PARAMETER ------------------------------------#

#generate impact parameter based on Hut & Bahcall 1983 http://articles.adsabs.harvard.edu/pdf/1983ApJ...268..319H

#this time respect to previous sim we set bmax as computed for each sim

#NB if fix cycle is not commented, the while cycle will produce b>D=100*a[i] and this will create an error in the x3,y3,z3 conversion equations (negative roots)



Bmax2 = ( 2 * G * (m1+m2+m3) * a ) / ( ( v * 1e5 ) * (yr/PC) )**2.                         #   pc^2 (v is in pc/yr)
    
impact  = []
count= []
check=0


i=0
while(i<Ntot):
    
    p=rnd.random()
    bmax2 = Bmax2[i]                                                                        #   pc^2
    p=uniformedev(p,bmin2,bmax2)
    p=p**0.5
    #impact.append(p)                                                                       #   COMMENT IF FIX CYCLE IS UNCOMMENTED
    
    
    #FIX CYCLE
    if p>100*a[i]:                                                                          #   when D<b we have a problem
        if check==0:
            count.append(i)
            check=1
    else:
        impact.append(p)
        check=0
        i+=1
    
    #i+=1                                                                                   #   COMMENT IF FIX CYCLE IS UNCOMMENTED
    
impact  = np.array(impact)
impact  *= L_scale                                                                          #   N-body conversion, it's already in parsec

b_max   = Bmax2**0.5


print(count)
print('Total number of resampled in b sist : '+str(len(count))+' , '+str(int(len(count)*100./Ntot))+'%% of the total')



#-------------------------------------DISTANCE PARAMETER------------------------------------#

#with the new prescription for b now i try to set D=100*a

D = 1000 * a * L_scale                                                                       #   pc

#the following cycle is just to check, the fix cycle in the impact paramenter should remove the problem
err_D=0
for i in range(len(D)):
    if D[i]<impact[i]:
       print('D < b !!!')
       D+=1
print('Errors in D = '+str(err_D))




#---------------------------------------  PSI ANGLE ----------------------------------------#

#generate psi, phi angle and theta based on Hut & Bahcall 1983 http://articles.adsabs.harvard.edu/pdf/1983ApJ...268..319H
#for theta they put a wrong value in Tab.1, see integral


psi=[]

i=0
while(i<Ntot):
    p=rnd.random()
    p=uniformedev(p,psimin,psimax)
    psi.append(p)
    i+=1



#---------------------------------------  PHI ANGLE ----------------------------------------#  



phi=[]

i=0
while(i<Ntot):
    p=rnd.random()
    p=uniformedev(p,phimin,phimax)
    phi.append(p)
    i+=1




#---------------------------------------  THETA ANGLE --------------------------------------#  



theta=[]

i=0
while(i<Ntot):
    p=rnd.random()
    p=uniformedev(p,costmin,costmax)
    p=np.arccos(p)
    theta.append(p)
    i+=1


print('D & Angles: DONE!')


#--------------------------------------------  SPINS ---------------------------------------#


spin = []




i     = 0

Spin = np.zeros((Ntot,9))                                                                   #   ¢reate empty array each row is a simulation
                                                                                            #   columns are sx1,sy1,sz1,sx2,sy2,sz2,sx3,sy3,sz3
while(i<(Ntot)):
    
    j    = 0
    row  = []
    
    while j<3:                                                                              #   3 black holes for each sim      
        
        # spin magnitude #
        
        p=[]
        k=0
        while(k<3):
            p1=rnd.random()
            p2=rnd.random()
            p.append(gauss(p1,p2,Svmean,Svstd))
            k+=1
        S = (p[0]*p[0]+p[1]*p[1]+p[2]*p[2])**0.5
        spin.append(S)
    
        # angles #
        
        Phi       = uniformedev(rnd.random(),0.0,2.*math.pi)
        cos_theta = uniformedev(rnd.random(),-1.0,1.0)
        Theta     = np.arccos(cos_theta)
    
        # projections #
    
        Sx = S * np.sin(Theta) * np.cos(Phi)                                                #   adimensionals
        Sy = S * np.sin(Theta) * np.sin(Phi)
        Sz = S * np.cos(Theta)
        
        row.append(Sx)
        row.append(Sy)
        row.append(Sz)   
        j+=1
        

    Spin[i,:] = row
    i+=1
    
print('Spins: DONE!')


######################################### CHECK-POINT 1 #################################### 

if check1:

    stream=open("orbital_parameters_YSC.dat","w");
    for i in range(len(impact)):
        if i==0:
            stream.write(str('Integration_time = ')+str(int_t)+str(' [yrs]')+' - '+str(Int_time)+str(' [N_units]')+'   '+str('Step_time = ')+str(step_t)+str(' [years]')+' - '+str(Step_time)+str(' [N_units]')+'   /   Speed of Light [N_units] ='+str(c*V_scale)+'\n')
            stream.write(str('N_sim')+' '+str('m1       m2      m3      a[pc]     e      b[pc]      b_max[pc]      phi       theta       psi        f       v[km/s]         Ecc_Anomaly         a_hard [pc]         a_ej [pc]           a_gw [pc]')+'\n')
            stream.write(str('%d'%(i+Ngen))+' '+str(max(m1[i],m2[i]))+' '+str(min(m1[i],m2[i]))+' '+str(m3[i])+' '+str(a[i])+' '+str(ecc[i])+' '+str(impact[i])+' '+str(b_max[i])+'  '+str(phi[i])+' '+str(theta[i])+' '+str(psi[i])+' '+str(fase[i])+' '+str(v[i])+' '+str(Efinale[i])+' '+str(A_hard[i])+' '+str(A_ej[i])+' '+str(A_gw[i])+'\n')
        else:
            stream.write(str('%d'%(i+Ngen))+' '+str(max(m1[i],m2[i]))+' '+str(min(m1[i],m2[i]))+' '+str(m3[i])+' '+str(a[i])+' '+str(ecc[i])+' '+str(impact[i])+' '+str(b_max[i])+'  '+str(phi[i])+' '+str(theta[i])+' '+str(psi[i])+' '+str(fase[i])+' '+str(v[i])+' '+str(Efinale[i])+' '+str(A_hard[i])+' '+str(A_ej[i])+' '+str(A_gw[i])+'\n')


#################################### CHANGE OF COORDINATES #################################

if check2:

    #m1 (m2)=primary (secondary) member of binary, m3=intruder
    #pedex 1,2,3 always indicate primary member, secondary member, intruder
        
    x1=np.zeros(Ntot,float)
    y1=np.zeros(Ntot,float)
    z1=np.zeros(Ntot,float)

    vx1=np.zeros(Ntot,float)
    vy1=np.zeros(Ntot,float)
    vz1=np.zeros(Ntot,float)

    x2=np.zeros(Ntot,float)
    y2=np.zeros(Ntot,float)
    z2=np.zeros(Ntot,float)

    vx2=np.zeros(Ntot,float)
    vy2=np.zeros(Ntot,float)
    vz2=np.zeros(Ntot,float)

    x3=np.zeros(Ntot,float)
    y3=np.zeros(Ntot,float)
    z3=np.zeros(Ntot,float)

    vx3=np.zeros(Ntot,float)
    vy3=np.zeros(Ntot,float)
    vz3=np.zeros(Ntot,float)

    i=0
    while(i<Ntot):
        
        M1    = max(m1[i],m2[i])
        M2    = min(m1[i],m2[i])
        
        x1[i] = -M2/(M1+M2) * a[i] * (1.-ecc[i]**2) * np.cos(fase[i])/(1.+(ecc[i] * np.cos(fase[i])))

        y1[i] = -M2/(M1+M2) * a[i] * (1.-ecc[i]**2) * np.sin(fase[i])/(1.+(ecc[i] * np.cos(fase[i])))

        z1[i] = 0.0
        
        vx1[i] = -M2/(M1+M2) * (ecc[i] * np.cos(fase[i]) /(1+ ecc[i] * np.cos(fase[i])) - 1.) * np.sin(fase[i]) * (1+ ecc[i] * np.cos(fase[i])) * (Gnbody * (M1+M2) / (a[i]*(1- ecc[i]*ecc[i])))**0.5
        
        vy1[i] = -M2/(M1+M2) * (ecc[i] * (np.sin(fase[i]))**2 /(1+ ecc[i] * np.cos(fase[i])) + np.cos(fase[i])) * (1+ ecc[i] * np.cos(fase[i])) * (Gnbody * (M1+M2) / (a[i]*(1- ecc[i]*ecc[i])))**0.5                                      

        vz1[i] = 0.0
        
        x2[i] = M1/(M1+M2) * a[i] * (1.-ecc[i]**2) * np.cos(fase[i])/(1.+(ecc[i] * np.cos(fase[i])))

        y2[i] = M1/(M1+M2) * a[i] * (1.-ecc[i]**2) * np.sin(fase[i])/(1.+(ecc[i] * np.cos(fase[i])))

        z2[i] = 0.0
        
        vx2[i] = M1/(M1+M2) * (ecc[i] * np.cos(fase[i]) /(1+ ecc[i] * np.cos(fase[i])) - 1.) * np.sin(fase[i]) * (1+ ecc[i] * np.cos(fase[i])) * (Gnbody * (M1+M2) / (a[i]*(1- ecc[i]*ecc[i])))**0.5 

        vy2[i] = M1/(M1+M2) * (ecc[i] * (np.sin(fase[i]))**2 /(1+ ecc[i] * np.cos(fase[i])) + np.cos(fase[i])) * (1+ ecc[i] * np.cos(fase[i])) * (Gnbody * (M1+M2) / (a[i]*(1- ecc[i]*ecc[i])))**0.5                                     

        vz2[i] = 0.0

        
        x3[i] = D[i] * ( np.sin(phi[i]) * (impact[i]/D[i]) * np.cos(psi[i]) - np.cos(phi[i]) * ( (1.0 - (impact[i]/D[i])**2)**0.5 * np.sin(theta[i]) +  (impact[i]/D[i]) * np.cos(theta[i]) * np.sin(psi[i])))

        y3[i] = -D[i] * ( np.sin(phi[i]) * ((1.0 - (impact[i]/D[i])**2)**0.5 * np.sin(theta[i]) + (impact[i]/D[i]) * np.cos(theta[i]) * np.sin(psi[i])) + np.cos(phi[i]) * np.cos(psi[i]) * (impact[i]/D[i]))

        z3[i] =  D[i] * ( -np.cos(theta[i]) * (1.0- (impact[i]/D[i])**2 )**0.5 + (impact[i]/D[i]) * np.sin(theta[i]) * np.sin(psi[i]))     

        vx3[i] = vel[i] * np.sin(theta[i]) * np.cos(phi[i])
        vy3[i] = vel[i] * np.sin(theta[i]) * np.sin(phi[i])
        vz3[i] = vel[i] * np.cos(theta[i]) 
        i+=1

    ###################################### CHECK-POINT 2 ####################################### 

    #   NB REMEMBER: these are the input files for ARWV, the units are Msun, yr, pc -->Nunits

    i=0
    for i in range(Ntot):
        filename = 'TBS_%d.dat'%(i+Ngen)
        stream2=open(filename,"w");
        stream2.write(str(max(m1[i],m2[i]))+' '+str(x1[i])+' '+str(y1[i])+' '+str(z1[i])+' '+str(vx1[i])+' '+str(vy1[i])+' '+str(vz1[i])+'\n'+str(min(m1[i],m2[i]))+' '+str(x2[i])+' '+str(y2[i])+' '+str(z2[i])+' '+str(vx2[i])+' '+str(vy2[i])+' '+str(vz2[i])+'\n'+str(m3[i])+' '+str(x3[i])+' '+str(y3[i])+' '+str(z3[i])+' '+str(vx3[i])+' '+str(vy3[i])+' '+str(vz3[i])+'\n')
        stream2.close()
        
    i=0
    while(i<(Ntot)):
        filename = 'spin_%d.dat'%(i+Ngen)
        stream=open(filename,"w");
        stream.write(str(Spin[i,0])+' '+str(Spin[i,1])+' '+str(Spin[i,2])+'\n'+str(Spin[i,3])+' '+str(Spin[i,4])+' '+str(Spin[i,5])+'\n'+str(Spin[i,6])+' '+str(Spin[i,7])+' '+str(Spin[i,8])+'\n')
        stream.close()
        i+=1
    


################################### CHECK PLOTS #########################################   

if infoplot:

    n_bins=75

    fig,ax=plt.subplots(4,2)
    ax[0][0].hist(impact, bins=n_bins)
    ax[0][0].set_xlabel('$b [pc]$')

    ax[1][0].hist(phi, bins=n_bins)
    ax[1][0].set_xlabel('$\phi{}$ (rad)')


    ax[2][0].hist(theta, bins=n_bins)
    ax[2][0].set_xlabel('$\\theta{}$ (rad)')

    ax[3][0].hist(ecc,bins=n_bins)
    ax[3][0].set_xlabel('eccentricity')

    ax[0][1].hist(psi, bins=n_bins)
    ax[0][1].set_xlabel('$\psi{}$ (rad)')


    ax[1][1].hist(fase, bins=n_bins)
    ax[1][1].set_xlabel('phase (rad)')

    ax[2][1].hist(v, bins=n_bins)
    ax[2][1].set_xlabel('$v$ [km/s]')

    ax[3][1].hist(Efinale,bins=n_bins)
    ax[3][1].set_xlabel('Eccentric anomaly')

    plt.tight_layout()





    fig2,axs=plt.subplots(2,2)
    axs[0][0].plot(x1,y1,'r.')
    axs[0][0].plot(x2,y2,'b.')
    axs[0][0].plot(x3,y3,'g.')
    axs[0][0].set_xlabel('$x$ [pc]')
    axs[0][0].set_ylabel('$y$ [pc]')


    axs[1][0].plot(x1,z1,'r.')
    axs[1][0].plot(x2,z2,'b.')
    axs[1][0].plot(x3,z3,'g.')
    axs[1][0].set_xlabel('$x$ [pc]')
    axs[1][0].set_ylabel('$z$ [pc]')


    axs[0][1].plot(vx1*((PC/yr)/(V_scale*1e5)),vy1*((PC/yr)/(V_scale*1e5)),'r.')
    axs[0][1].plot(vx2*((PC/yr)/(V_scale*1e5)),vy2*((PC/yr)/(V_scale*1e5)),'b.')
    axs[0][1].plot(vx3*((PC/yr)/(V_scale*1e5)),vy3*((PC/yr)/(V_scale*1e5)),'g.')
    axs[0][1].set_xlabel('$v_x$ [km/s]')
    axs[0][1].set_ylabel('$v_y$ [km/s]')


    axs[1][1].plot(vx1*((PC/yr)/(V_scale*1e5)),vz1*((PC/yr)/(V_scale*1e5)),'r.')
    axs[1][1].plot(vx2*((PC/yr)/(V_scale*1e5)),vz2*((PC/yr)/(V_scale*1e5)),'b.')
    axs[1][1].plot(vx3*((PC/yr)/(V_scale*1e5)),vz3*((PC/yr)/(V_scale*1e5)),'g.')
    axs[1][1].set_xlabel('$v_x$ [km/s]')
    axs[1][1].set_ylabel('$v_z$ [km/s]')


    plt.tight_layout()


    fig3,ax = plt.subplots(2,2)



    ax[0][0].hist(a, bins=n_bins)
    ax[0][0].set_xlabel('Semi-major axis ')

    ax[1][0].hist(m1, bins=n_bins)
    ax[1][0].set_xlabel('$M_1$ ')

    ax[1][1].hist(m2, bins=n_bins)
    ax[1][1].set_xlabel('$M_2$ ')

    ax[0][1].hist(m3, bins=n_bins)
    ax[0][1].set_xlabel('$M_3$ ')



    fig = plt.subplots(2,1,figsize=(16,12))
    gs = gridspec.GridSpec(2, 1, height_ratios=[1, 1])

    ax0 = plt.subplot(gs[0])
    ax0.hist(impact/a,bins=n_bins,alpha=0.5, histtype='stepfilled',ec="k")
    ax0.set_ylabel('N', size=24)
    ax0.set_xlabel('b/a', size=24)
    ax0.set_yscale('log')
    ax0.tick_params(axis="x", labelsize=19)
    ax0.tick_params(axis="y", labelsize=19)
    ax0.tick_params(which='both', width=1)
    ax0.tick_params(which='major', length=6)

    ax1 = plt.subplot(gs[1])
    ax1.hist(b_max/a,bins=n_bins,alpha=0.5, histtype='stepfilled',ec="k")
    ax1.set_ylabel('N', size=24)
    ax1.set_xlabel('$b_{max}$/a', size=24)
    ax1.set_yscale('log')
    ax1.tick_params(axis="x", labelsize=19)
    ax1.tick_params(axis="y", labelsize=19)


    plt.savefig('b_over_a.png', dpi=500, bbox_inches='tight')




    plt.show()
